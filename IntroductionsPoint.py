from stem.control import Controller

CONTROL_PORT = 9151

controller =  Controller.from_port(port=CONTROL_PORT)
controller.authenticate()

service = controller.get_hidden_service_descriptor('danschatjr7qbwip')

for relay in service.introduction_points():
    print(relay.address, relay.port, relay.identifier)
    
    
    #This is going to Print your Nodes you are on
