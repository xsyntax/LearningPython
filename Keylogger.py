import sys
from Xlib import X, XK, display
from Xlib.ext import record
from Xlib.protocol import rq

# Grab display
display = display.Display()

# Listen for these events
events = (X.KeyPress, X.ButtonPress)

# Create context object
ctx = display.record_create_context(
    0,
    [record.AllClients],
    [
        {
            'core_requests': (0, 0),
            'core_replies': (0, 0),
            'ext_requests': (0, 0, 0, 0),
            'ext_replies': (0, 0, 0, 0),
            'delivered_events': (0, 0),
            'device_events': events,
            'errors': (0, 0),
            'client_started': False,
            'client_died': False,
        }
    ]
)

# Lookup keysym
def lookup(key):
    for name in dir(XK):
        if name.startswith("XK_") and getattr(XK, name) == key:
            return name.lstrip("XK_")
    return None

# Do this on each display event
def on_reply(reply):
    data = reply.data
    while data:
        # Parse out the event data
        field = rq.EventField(None)
        event, data = field.parse_binary_value(data, display.display, None, None)
        if event.type == X.KeyPress:
            # Lookup and then write the key to stdout
            sym = display.keycode_to_keysym(event.detail, 0)
            key = lookup(sym)
            sys.stdout.write(key)
            sys.stdout.write(' ')
            sys.stdout.flush()

# Start the listener
display.record_enable_context(ctx, on_reply)