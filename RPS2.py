import random
chars = 'RPS'
while True:
    ai = random.choice(chars)
    move = input('(R)ock (P)aper (S)cissors: ')
    if not move or move not in chars:
        break
    print('AI played ' + ai)
    if move == ai:
        print('Tie!')
    elif chars.index(move) == (chars.index(ai) + 1) % 3:
        print('You win!')
    else:
        print('You lose!')