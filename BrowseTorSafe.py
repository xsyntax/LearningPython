'''
This code will close any circuits that route through any of the danwin relays.
His nodes are no longer considered safe.
'''

from stem.control import Controller

CONTROL_PORT = 9151

controller =  Controller.from_port(port=CONTROL_PORT)
controller.authenticate()

for circuit in controller.get_circuits():
    print('Circuit', circuit.id)
    for relay in circuit.path:
        name = relay[1]
        if 'danwin' in name.lower():
            print('closing circuit', circuit.id)
            controller.close_circuit(circuit.id)
