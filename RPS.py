import random

def Rock_Paper_Scissors():
    AI = random.randint(1,3)
    if AI == 1:
     AI_Rock()
    elif AI == 2:
     AI_Paper()
    elif AI == 3: 
     AI_Scissors()

def AI_Rock():
    choice = input("1 is Rock,2 is Paper,3 is Scissors:")
    if choice == "1":
       print("You matched AI, both picked ROCK!")
       try_again()
    if choice == "2":
       print("You killed it, You picked Paper, AI choose Rock!")
       try_again()
    if choice == "3":
       print("You're a loser, You picked Scissors, AI picked Rock!")
       try_again()
    else:
        print("Try Again")
        AI_Rock()

def AI_Paper():
    choice = input("1 is Rock,2 is Paper,3 is Scissors:")
    if choice == "1":
       print("You're a loser, You picked Rock, and AI picked Paper!")
       try_again()
    if choice == "2":
       print("You Matched, you both picked Paper!")
       try_again()
    if choice == "3":
       print("You killed it, you picked Scissors, AI picked Paper!")
       try_again()
    else:
        print("Try Again")
        AI_Paper()

def AI_Scissors():
    choice = input("1 is Rock,2 is Paper,3 is Scissors:")
    if choice == "1":
       print("You killed it, You picked Rock, and AI picked Scissors!")
       try_again()
    if choice == "2":
       print("You're a loser, you picked Paper, AI picked Scissors!")
       try_again()
    if choice == "3":
       print("You Matched, you both picked Scissors!")
       try_again()
    else:
        print("Try Again")
        AI_Scissors()

def try_again():
    choice = input("PLay again? yes/no")
    if choice == "yes" :
        Rock_Paper_Scissors()
    elif choice == "no" :
       print("Thanks for playing")
       quit()
    else:
       print("Wrong, Try Again")
       try_again()

Rock_Paper_Scissors()
